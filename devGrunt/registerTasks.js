/**
 * @Author: thiemo
 * @Date:   2017-10-08T18:57:38+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2017-12-01T16:18:21+01:00
 */



module.exports = function(grunt){
  //These are the tasks that can be run with grunt
  grunt.registerTask('serve', ['default','connect','watch']);

  grunt.registerMultiTask('collectAssets','This task collects all assets for preload', function(){
    grunt.file.write(this.data.files[0].dest,JSON.stringify({ manifest: (this.filesSrc.map(
      function(e)
      {
        if(!grunt.file.isDir(e)) return e.substr(e.indexOf("/")+1);
      }).filter(function(e){return e!=null;}))}));
  });
  // default task is executed when grunt is run without parameters.
  // uglify task is not included, run it explicitely for production build
  grunt.registerTask('default', ['copy:assetsImg', 'copy:assetsEssentials' , 'concat', 'collectAssets']);

  // grunt.registerTask('addsettings', ['copy:manifest']);
}
