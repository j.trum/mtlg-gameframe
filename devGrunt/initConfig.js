/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-08-10T14:30:10+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-12-08T14:09:52+01:00
 */



module.exports = function(grunt) {
  //framework_root = "./mtlg-gameframe";
  //framework_root = "../../mtlg-gameframe";
  framework_root = "./node_modules/mtlg-gameframe";

  // initialize grunt for project
  grunt.initConfig({
    // copy assets
    copy: {
      /*manifest: {
        files: [{
          expand: true,
          cwd: `${framework_root}/dist/dev/manifest/`,
          src: 'game.settings.js',
          dest: 'dev/manifest/'
        }]
      },*/
      assetsImg: {
        files: [{
            expand: true,
            cwd: 'dev/',
            src: 'img/**',
            dest: 'build/'
          },
          {
            expand: true,
            cwd: 'dev/',
            src: 'sounds/**',
            dest: 'build/'
          },
          {
            expand: true,
            cwd: 'dev/',
            src: 'manifest/**',
            dest: 'build/'
          },
          {
            expand: true,
            cwd: `${framework_root}/`,
            src: 'img/**',
            dest: 'build/'
          },
          {
            expand: true,
            cwd: `${framework_root}/`,
            src: 'sounds/**',
            dest: 'build/'
          },
        ],
      },
      assetsEssentials: {
        files: [
          {expand: true, cwd: `${framework_root}/build_essentials/`, src: 'index.html', dest: 'build/'},
          {expand: true, cwd: `${framework_root}/build_essentials/`, src: '.gitignore', dest: './'},
          {expand: true, cwd: `${framework_root}/build_essentials/`, src: 'extern/**', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/framework/`, src: 'mtlg.js', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/modules/`, src: '*.js', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/modules/mtlg-modul-tangibles/`, src: '*.js', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/modules/mtlg-moduls-utilities/`, src: '*.js', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/modules/mtlg-modul-menu/`, src: '*.js', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/modules/mtlg-modul-distributeddisplays/`, src: '*.js', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/modules/mtlg-modul-tabulaEvents/`, src: '*.js', dest: 'build/lib/'},
          {expand: true, cwd: `${framework_root}/lib/modules/mtlg-modul-menu/demo/assets/img/module/menu/`, src: '*.svg', dest: 'build/assets/img/module/menu/'},
          {expand: true, cwd: `${framework_root}/db/`, src: '*', dest: 'build/db/'}
        ],
      },
    },

    // merge multiple source files into one destination file
    concat: {
      options: {
        separator: '\n',
      },
      developer: {
        src: [`${framework_root}/lib/framework/modul-wrapper/game-header.js`, 'dev/js/**/*.js', `${framework_root}/lib/framework/modul-wrapper/game-footer.js`],
        dest: 'build/lib/game.js',
      },
      css: {
        src: [`${framework_root}/build_essentials/css/main.css`, `${framework_root}/lib/modules/mtlg-modul-menu/menu.css`],
        dest: 'build/assets/css/game.css',
      },
      langDefs: {
        src: ['dev/lang/*.js'],
        dest: 'build/lib/langDefs.js',
      }
    },

    connect: {
      server: {
        options: {
          port: 4000,
          base: 'build',
          hostname: '*'
        }
      }
    },

    watch: {
      assetsImg: {
        files: 'dev/img/**',
        tasks: ['collectAssets']
      },
      assetsSnd: {
        files: 'dev/sounds/**',
        tasks: ['collectAssets']
      },
      code: {
        files: 'dev/**',
        tasks: ['copy', 'concat']
      }
    },

    collectAssets: {
      images: {
        files: [{
          expand: true,
          cwd: 'build/img',
          src: ['**', '!read.me'],
          dest: 'build/imageassets.json'
        }]
      },
      sounds: {
        files: [{
          expand: true,
          cwd: 'build/sounds',
          src: ['**/*', '!read.me'],
          dest: 'build/soundassets.json'
        }]
      }
    },

    // minify javascript files for production
    uglify: {
      frameworkMin: {
        files: {
          'build/lib/mtlg.min.js': ['build/lib/mtlg.js']
        }
      },
      langDefsMin: {
        files: {
          'build/lib/langDefs.min.js': ['build/lib/langDefs.js']
        }
      },
      gameMin: {
        files: {
          'build/lib/game.min.js': ['build/lib/game.js']
        }
      }
    }
  });
}
