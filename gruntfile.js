/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2016-11-11T11:20:08+01:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-11-03T10:47:59+01:00
 */

child_process = require('child_process');

/*global module:false*/
module.exports = function(grunt) {

  framework_root = "./node_modules/MTLG";

  // initializes Grunt with following configuration
  grunt.initConfig({

    copy: {
      game: {
        files: [{
          dot: true,
          expand: true,
          cwd: `${framework_root}/dist/`,
          src: ['**', '!**/templates/**'],
          dest: './'
        }, ],
      },
    },

		shell: {
        options: {
            stderr: false
        },
        init: {
            command: 'git submodule update --init --recursive'
        },
				fetch: {
            command: 'git submodule foreach --recursive git fetch'
        },
				master: {
            command: 'git submodule foreach --recursive git merge origin master'
        },
    }
  });

  require('grunt-contrib-copy')(grunt);
  require('load-grunt-tasks')(grunt);

  grunt.registerTask('auto_install', function() {
    var done = this.async();
    child_process.spawn('npm', ['install'], {
        stdio: 'inherit',
        shell: true
      })
      .on('close', (code) => done(code == 0));
  });

  grunt.registerTask('copy_newgame', ['copy', 'auto_install']);

  grunt.registerTask('default', 'My "default" task description.', function() {
    grunt.log.writeln('In this grunt config you can only set up new games. Type grunt new_game:[name of the game without brackets]');
  });

  grunt.registerTask('new_game', 'Setup a new game directory.', function() {
    grunt.task.run('copy_newgame');

    //Read and change template values of gitlabci and gitignore
    //Make sure that no old gitlabci exists
    //If it does: Ignore it. We assume that the changed gitlabci is better
    if (!grunt.file.exists('.gitlab-ci.yml')) { //Does not exist
      console.log("Creating new gitlabci");
      var gitlabci = grunt.file.read(`${framework_root}/dist/templates/gitlabci`);
      gitlabci = grunt.template.process(gitlabci);
      grunt.file.write('.gitlab-ci.yml', gitlabci); //Copy to parent directory, assume submodule
    } else {
      console.log("Using existing gitlabci");
    }
    //Make sure that no old gitignore exists
    //If it does: Add the new lines to the bottom if they do not yet exist
    var oldGitignore;
    if (grunt.file.exists('.gitignore')) {
      console.log("Extending old gitignore");
      oldGitignore = grunt.file.read('.gitignore');
      //Read template to get the columns we are looking for
      var gitignore = grunt.file.read(`${framework_root}/dist/templates/gitignore`);
      gitignore = grunt.template.process(gitignore);
      //Create regex for each line of the gitignore and match it to the old gitignore
      var lines = gitignore.split("\n");
      var i;
      for (i = 0; i < lines.length; i++) {
        var rege = new RegExp("^" + lines[i] + "$[\n]?", "mg"); //Only delete lines that are identical, no pre/suffixes
        oldGitignore = oldGitignore.replace(rege, "");
      }
      //Append the template and write the file
      oldGitignore = oldGitignore + "\n" + gitignore;
      grunt.file.write('.gitignore', oldGitignore);
    } else { //Simply use the template
      console.log("Creating new gitignore");
      var gitignore = grunt.file.read(`${framework_root}/dist/templates/gitignore`);
      oldGitignore = grunt.template.process(gitignore);
      grunt.file.write('.gitignore', oldGitignore);
    }
    //Make sure MTLG dependency remains in package.json the way it was installed
    package_json = JSON.parse(grunt.file.read(`${framework_root}/dist/templates/package.json`))
    try {
      //second prio: fixed commit as reported in package.lock.json
      var framework_version = JSON.parse(grunt.file.read('package-lock.json')).dependencies['MTLG'].version
      //first prio: as defined in package.json
      framework_version = JSON.parse(grunt.file.read('package.json')).dependencies['MTLG']
    } catch (e) {}
    if (framework_version) {
      if (package_json.dependencies === undefined) {
        package_json.dependencies = {
          "MTLG": framework_version
        };
      } else {
        package_json.dependencies["MTLG"] = framework_version;
      }
    }
    grunt.file.write('package.json', JSON.stringify(package_json, null, "  "))
  });


  grunt.registerTask('update_sub', ['shell']);
};
