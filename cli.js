#!/usr/bin/env node

switch(process.argv[2]){
  case "new_game":
    grunt = require("grunt");
    require("./gruntfile.js")(grunt);
    grunt.task.init = function() {};
    grunt.tasks(['new_game']);
    break;
  default:
    console.log(`
usage: mtlg new_game
    `);
}
