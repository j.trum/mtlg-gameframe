/*
 * These variables should always be present and used if necessary.
 */
var options, //game options read from manifest files
  gameData, //game state, level info...
  areas, //Information about user areas.
  sharedArea, //Free area left
  players, //An array containing information about the players.
  firstRun; //A bool remembering if this is the first time the game is run

/*
 * These variables are game specific.
 */
var readyCounter;//Counts the number of players that are ready

//It is useful to redefine MTLG.lang.getString
//Write l(key) to get the corresponding string.
//The lang module is loaded after spiel, so the var is defined in init.
var l;

/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
var initGame = function(pOptions){
  //Initialize game
  //Set default var values
  setupVars(pOptions);

  //Initialize levels and Menus:
  //The MTLG framework uses the lifecycle manager (lc) to handle level progression,
  //resetting levels and to define a starting point for the game.
  //Use the functions registerMenu and registerLevel to define your levels.
  //The starting point of the game will be the registered menu, if one is present,
  //or the game with the highest value when passed the empty game state.
  //Register Menu
  MTLG.lc.registerMenu(drawMainMenu);

  //Register levels
  //Levels consist of two parts: the first parameter is the entrance function,
  //that sets the level up and renders it. The second parameter as a check function,
  //that receives the current gamestate as paramter and returns a value between 0 and 1.
  //The lifecycle manager starts the level with the highest return value in the next step.
  MTLG.lc.registerLevel(field1_init, checkLevel1);
  MTLG.lc.registerLevel(field2_init, checkLevel2);
  MTLG.lc.registerLevel(drawFeedback, checkMyFeedback);

  //Define l as abbreveation  for translations.
  l = MTLG.lang.getString;

  //Init is done
  console.log("Game Init function called");
}

//Register Init function with the MTLG framework
//The function passed to addGameInit will be used to initialize the game.
//Use the initialization to register levels and menus.
MTLG.addGameInit(initGame);

/*
* sets parameters and optionally does further instantiations
*
* pOptions contain:
* language: game's language
* width: width of playing field in browser's full screen mode
* height: height of playing field in browser's full screen mode
* countdown: timespan that must pass without anything happening, before game requests a restart
* fps: preset FPS for TweenJS
*/
var setupVars = function(pOptions)
{
    options = pOptions;
    gameData = {};
    readyCounter = 0;
    areas = [];
    sharedArea = {};
    players = []; //This can be populated in the first level after MTLG.goToLogin was called.
    firstRun = true;
};
