/*
* draws the second playing area
*/
var field2_init = function()
{
  //MTLG.createContainers can be used to create containers that cover the stage
  //in a predefined pattern. This can be useful to draw the same level for several
  //players.
  areas = MTLG.createContainers(MTLG.getPlayerNumber());
  setup_field2_vars(areas.length);
  //We created a container for every player, now we iterate over the containers
  //and draw the level.
  for(var i = 0; i < areas.length; i++){
    areas[i].player = MTLG.getPlayer(i);
    drawField2(areas[i]);
    MTLG.getStage().addChild(areas[i]);
  }
  startTimeMeasurement();
};

/*
* draws second playing field in the given area
* @param area: area which to draw into
*/
var drawField2 = function(area)
{
    var titletext = l('title_field2');
    var title = new createjs.Text(area.player.name +" "+titletext, '30px Arial', 'white');
    title.lineWidth = area.getBounds().width * 5/6;//Use this to make lines wrap if the text is too long
    title.x = 10;
    title.y = 40;
    title.textBaseline = 'alphabetic';

    var background = new createjs.Shape();
    background.graphics.beginFill('black').drawRect(0, 0, area.getBounds().width, area.getBounds().height);
    background.x = 0;
    background.y = 0;

    var distance = (60/areas.length);
    var dimension = (200/areas.length);

    area.addChild(background);
    area.addChild(title);

    var animationsVar = 0;
    var duration = 250;

    for(var i = 0; i < 4; i++){
        boxes[area.index][i] = new createjs.Shape();
        boxes[area.index][i].graphics.beginFill('white').drawRect(0, 0, dimension, dimension);

        var text;
        switch(i){
            case 0: text = 1; break;
            case 1: text = 2; break;
            case 2: text = 4; break;
            case 3: text = 3; break;
            default: console.log('prolem assigning number to the boxes on field2');
        }

        (function(j){
            boxes[area.index][i].addEventListener('click', function(event){
                boxClick(event.currentTarget, area.index, j);
            });
        }(text));

        var variableTextSize = Math.ceil(dimension * 3 / 4);
        var number = new createjs.Text(text, variableTextSize + 'px Arial', 'red');
        number.x = ((distance * 1.5) + i*(distance + dimension));
        number.y = ((area.getBounds().height/2)-(dimension/2)+10);

        boxes[area.index][i].x = (distance + i*(distance + dimension));
        boxes[area.index][i].y = ((area.getBounds().height/2)-(dimension/2));
      var coordinates = {xCoord : (distance + i*(distance + dimension)), yCoord : ((area.getBounds().height/2)-(dimension/2)) };

        area.addChild(boxes[area.index][i]);
        area.addChild(number);

        /*
        * animation demonstration
        */
        if(animationsVar === 0){
            boxes[area.index][i].y = 0;
            createjs.Tween.get(boxes[area.index][i]).to({y: coordinates.yCoord}, duration, createjs.Ease.circOut);
            animationsVar++;
        }
        else if(animationsVar === 1){
            boxes[area.index][i].x = 0;
            createjs.Tween.get(boxes[area.index][i]).to({x: coordinates.xCoord}, duration, createjs.Ease.circOut);
            animationsVar++;
        }
        else if(animationsVar === 2){
            boxes[area.index][i].x = area.getBounds().width - dimension;
            createjs.Tween.get(boxes[area.index][i]).to({x: coordinates.xCoord}, duration, createjs.Ease.circOut);
            animationsVar++;
        }
        else if(animationsVar === 3){
            boxes[area.index][i].y = area.getBounds().height;
            createjs.Tween.get(boxes[area.index][i]).to({y: coordinates.yCoord}, duration, createjs.Ease.circOut);
        }
    }
};
