//which number was last pressed
var lastNumber = [];
//contains drawed boxes
var boxes = [];
//start times of players
var tstart = [];
//end times of players
var tend = [];

/*
* sets lastNumber and boxes
*/
var setup_field2_vars = function()
{
    for(var i = 0; i < areas.length; i++){
        lastNumber[i] = 0;
        boxes[i] = [];
    }
    readyCounter = 0;
};

/*
* starts time measurement
*/
var startTimeMeasurement = function()
{
    for(var i = 0; i < areas.length; i++){
        tstart[i] = new Date().getTime();
    }
};

/*
* processes clicks on boxes
* The function checks whether the right sequence of numbers was clicked.
* It checks whether the player is ready and stops the time.
* If all players have finished, the feedback phase is initiated.
* @param object: clicked box
* @param areaIndex: index of clicked area (area.index)
* @param number: Die angeklickte Nummer
*/
var boxClick = function(object, areaIndex, number)
{
    if(lastNumber[areaIndex] === (number-1)){
        lastNumber[areaIndex] = number;
        object.graphics._fill.style = 'yellow';
        if(number === 4){
            tend[areaIndex] = new Date().getTime();
            readyCounter++;
            if(readyCounter === areas.length){
              MTLG.lc.levelFinished({nextLevel : 3, done : 1});
            }
            else{
                draw_temp_feedback(areaIndex);
            }
        }
    }
    else {
        for(var i = 0; i < boxes[areaIndex].length; i++){
            boxes[areaIndex][i].graphics._fill.style = 'white';
        }
        lastNumber[areaIndex] = 0;
    }
};

/*
* Draws temporary feedbackscreen for the player, that first finished
* The time of each player is shown in milliseconds
* @param areaIndex: index of area, whose player first finished
*/
var draw_temp_feedback = function(areaIndex)
{

  area = areas[areaIndex];
    var background = new createjs.Shape();
    background.graphics.beginFill('gray').drawRect(0, 0, area.getBounds().width, area.getBounds().height);
    background.alpha = 0.8;
    background.x = 0;
    background.y = 0;
    background.addEventListener('click', function(event){
        //Overlay catches clickevents and does nothing more
    });

    var number = (tend[areaIndex] - tstart[areaIndex]);
    var letters = l('temp_feedback') + number;
    var text = new createjs.Text(letters, '30px Arial', 'red');
    text.maxWidth = area.getBounds().width -100;
    text.x = 50;
    text.y = 250;

    area.addChild(background);
    area.addChild(text);
};

/**
 * Function that checks if Field02 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLevel2 = function(gameState){
  if(!gameState){
    console.log("gameState undefined");
    return 0; //assuming we should not start
  }
  if(!gameState.nextLevel){
    console.log("gameState.nextLevel not set");
    console.log(gameState);
    return 0; //assuming we should not start
  }
  if(gameState.nextLevel === 2){
    return 1;
  } else {
    return 0;
  }
}
