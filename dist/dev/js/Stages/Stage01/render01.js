/*
* manages drawing into playing fields of every area
*/
var field1_init = function()
{
  //Login is over now, all players are registered with the MTLG framework
  //Use getPlayerNumber to get the number of players that are currently logged in.
  //Draw a welcome screen with all the players' names displayed
  //and a simple task to start the game (drag yellow rectangle into white area)
  var playerNames = "";
  for(var i = 0; i < MTLG.getPlayerNumber(); i++){
    //The players can be set now
    //To get more detailed information on the players, use the gibSpieler function.
    players.push(MTLG.getPlayer(i));
    playerNames += players[i].name + "\n";
  }

  //Create a large container for the stage
  //This container can be used as a starting point for creating game stages.
  //The function drawField1 draws the level using the container.
  areas = MTLG.createContainers(1);
  drawField1(areas[0],playerNames);
  MTLG.getStage().addChild(areas[0]);
  readyCounter = 0;
};

/*
* this function draws the first playing field in the passed area
* @param area: area to draw in
*/
var drawField1 = function(area,playerNames)
{
    var stage = MTLG.getStage();

    //Text
    var text = l('title_field1');
    var variableTextSize = Math.ceil(area.getBounds().height / 30);
    var title = new createjs.Text(playerNames + text, variableTextSize + 'px Arial', 'red');
    title.maxWidth = area.getBounds().width * 5/6; //Compress font size to make the text fit
    //title.lineWidth = area.getBounds().width * 5/6;//Use this instead to make lines wrap instead of compressing
    title.x = 25;
    title.y = 55;
    title.textBaseline = 'alphabetic';

    //Blue Background Rectangle
    var background = new createjs.Shape();
    background.graphics.beginFill('blue').drawRect(0, 0, area.getBounds().width, area.getBounds().height);
    background.regX = area.getBounds().width / 2;
    background.regY = area.getBounds().height / 2;
    background.rotation = area.rotation;
    background.x = area, area.getBounds().width/2;
    background.y = area.getBounds().height/2;

    //absolute size demo
    var _5cm = MTLG.assets.getBitmap("img/5cm.png");
    _5cm.x = area.getBounds().width/2;
    _5cm.y = 70;
    MTLG.fixedSizeCm(_5cm, 5, 5);
    window._5cm = _5cm;

    //White zone Rect
    var zone = new createjs.Shape();
    var edge = area.getBounds().width * 0.1;
    zone.graphics.beginFill('white').drawRect(-edge/2, -edge/2, edge, edge);
    zone.setBounds(-edge/2, -edge/2, edge/2, edge/2);
    zone.x = area.getBounds().width*0.7;
    zone.y = area.getBounds().height/2;

    //Yellow object Rect
    var object = new createjs.Shape();
    var edge = area.getBounds().width * 0.05;
    object.graphics.beginFill('yellow').drawRect(-edge/2, -edge/2, edge, edge);
    object.setBounds(-edge/2, -edge/2, edge/2, edge/2);
    coordinates = MTLG.calculateCoordinates(area, area.getBounds().width * 0.3, (area.getBounds().height/2));
    object.x = area.getBounds().width * 0.3;
    object.y = area.getBounds().height/2;
    object.on('pressmove', function({localX, localY}){
        object.set(object.localToLocal(localX, localY, object.parent));
    });
    object.on('pressup', function({localX, localY}){
      object.set(object.localToLocal(localX, localY, object.parent));
      var conv = zone.localToLocal(localX, localY, object);
      if(zone.hitTest(conv.x,conv.y)){
        object.x = zone.x ; object.y = zone.y;
        object.removeAllEventListeners();
        //is level finished?
        readyCounter++;
        if(readyCounter === areas.length){
          MTLG.lc.levelFinished({nextLevel : 2});
        }
      }else{
        object.x = area.getBounds().width * 0.3;
        object.y = area.getBounds().height/2;
        //object.set(object.localToLocal(area.getBounds().width * 0.3,area.getBounds().height/2,object.parent));
      }
    });

    area.addChild(background);
    area.addChild(_5cm);
    area.addChild(title);
    area.addChild(zone);
    area.addChild(object);

};
