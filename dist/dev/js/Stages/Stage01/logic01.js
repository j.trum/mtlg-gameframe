/*
* checks, whether the object was dragged into target area
* @param object: dragged rectangle
* @param area: target area to which object is dragged
* @param curX: current X-coordinate of dragged object
* @param curY: current #-coordinate of dragged object
* @param tarX: X-coordinate of upper left corner of target area
* @param tarY: Y-coordinate of upper left corner of target area
* @param width: width of target area
* @param height: height of target area
*/
/*
var checkPosition = function(object, zone, area){

	var objectBounds = object.getBounds();
	var zoneBounds = zone.getBounds();
	if(object.parent)
		var p = object.parent.localToLocal(object.x, object.y, zone); // object's position
	// object inside zone
	if(    p.x + object.getBounds().x      > zone.getBounds().x
      && p.x + object.getBounds().width  < zone.getBounds().width
      && p.y + object.getBounds().y      > zone.getBounds().y
      && p.y + object.getBounds().height < zone.getBounds().height) {
    // move object inside zone
    object.x = zone.x ; object.y = zone.y;
    //is level finished?
    readyCounter++;
    if(readyCounter === areas.length){
      MTLG.lc.levelFinished({nextLevel : 2});
    }
    object.removeAllEventListeners();
    }
  else {
      //back to start position
      coordinates = MTLG.calculateCoordinates(area, area.width * 0.3, area.height/2);
      object.x = coordinates.xCoord;
      object.y = coordinates.yCoord;
  }
};
*/

/**
 * Function that checks if Feld01 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLevel1 = function(gameState){
  if(!gameState){
    console.log("gameState undefined");
    return 1; //assuming we should start anyway
  }
  if(!gameState.nextLevel){
    console.log("gameState.nextLevel not set");
    console.log(gameState);
    return 1; //assuming we should start anyway
  }
  if(gameState.nextLevel === 1){
    return 1;
  } else {
    return 0;
  }
}
