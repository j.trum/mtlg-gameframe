/**
 * @Author: thiemo
 * @Date:   2017-12-01T11:54:38+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2017-12-01T16:18:25+01:00
 */



window.MTLG = (function(m) {
  var modulsMTLG = {
    // add blocklist if needed default is load || if listed and set to 0 then module is bocked
    tabulaEvents: 0
  }

  m.loadModulsMTLG(modulsMTLG);
  return m;
})(window.MTLG);
