/**
 * @Author: thiemo
 * @Date:   2017-10-08T18:57:38+02:00
 * @Last modified by: Vincent
 * @Last modified time: 2017-11-03T13:27:08+01:00
 */



/**
 * The asset manager module takes care of preloading and serving assets like pictures and sound.
 * @namespace assets
 * @memberof MTLG
 */
var MTLG = (function(m){
  var q = new createjs.LoadQueue(true);
  var a = {};
  var moduleHandle = "assetManager";
  var imgAssetList ="imageassets.json";
  var soundAssetList = "soundassets.json";
  var mtlgCallback;
  var options;
  /**
   * @function init -
   * Initializes the module, starts the preload process.
   * Returns the moduleHandle since this modules is blocking.
   * @param {object} opt - the options currently available to the framework
   * @param {function} callback - the callback function that is called after the module finished loading
   * @return {string}  The module handle.
   * @memberof! MTLG.assets
   */
  a.init = function (opt, callback){
    q.addEventListener("complete", loadSounds);

    //Queue only knows about pictures, so divide progress by 2
    q.addEventListener("progress", function(ev){MTLG.drawProgress(ev.progress / 2);});
    q.loadManifest(imgAssetList);

    //Save options and callback
    mtlgCallback = callback;
    options = opt;
    return moduleHandle;


    //if(next) q.on("complete",next.handler,next.context);
  };

  function loadSounds(){
    q.removeAllEventListeners("complete");
    q.removeAllEventListeners("progress");
    //createjs.Sound.alternateExtensions = ["ogg"];
    q.installPlugin(createjs.Sound);
    q.addEventListener("complete", handleComplete);
    //Sounds are the last asset, so the progress is accurate
    q.addEventListener("progress", function(ev){MTLG.drawProgress(ev.progress);});
    q.loadManifest(soundAssetList);
  }

  function handleComplete(){
    q.removeAllEventListeners("complete");
    console.log("Finished Preloading")
    mtlgCallback(moduleHandle);
  }

  /**
   * @function getAsset -
   * Returns the requested asset.
   * @param {string} id - the id of the object that is requested. This is the path to the requested resource.
   * @return {object} - the asset that was requested
   * @memberof! MTLG.assets
   */
  a.getAsset = function(id){
    var instance = q.getResult(id);
    if(!instance){
      console.warn("Unable to provide preload resource: " + id);
    }
    return instance;
  }

  /**
   * @function getBitmap -
   * returns a cached createjs bitmap of the requested picture
   * @param {string} id - path to the picture
   * @return {object} a bitmap of the requested image or the empty bitmap if the requested image does not exist
   * @memberof! MTLG.assets
   */
  a.getBitmap = function(id){
    var image = a.getAsset(id);
    if(!image){
      return new createjs.Bitmap(null);
    } else {
      var bitMap = new createjs.Bitmap(image);
      if(bitMap.image.tagName != "VIDEO")
        bitMap.cache(0,0,bitMap.image.width,bitMap.image.height);
      return bitMap;
    }
  }

  /**
   * @function getBitmapScaled -
   * Returns a cached bitmap in a specific scale.
   * The scale is relative to the options.width and depends on the image width.
   * @param {string} id - path to image
   * @param {number} scale - new scale
   * @return {object} - bitmap of scaled image
   * @memberof! MTLG.assets
   */
  a.getBitmapScaled = function(id,scale){
    var instance = a.getBitmap(id);
    if(!instance){
      console.log("Unable to create bitmap from image: " + id);
      return null;
    }
    instance.scaleX = instance.scaleY = options.width * scale / instance.image.width;
    return instance;
  }

  /**
   * @function getBitmapAbsoluteScale -
   * Returns a cached bitmap in a specific scale.
   * The scale factor is in absolute values relative to the original.
   * Caches the bitmap.
   * @param {string} id - path to image
   * @param {scaleX} - value of scale on x axis
   * @param {scaleY} - value of scale on y axis
   * @return {object} The scaled and cached bitmap
   * @memberof! MTLG.assets
   */
  a.getBitmapAbsoluteScale = function(id,scaleX,scaleY){
    var instance = a.getBitmap(id);
    if(!instance){
      console.log("Unable to create bitmap from image: " + id);
      return null;
    }
    instance.scaleX = scaleX;
    instance.scaleY = scaleY;
    return instance;
  }


  /**
   * @function getSoundInstance -
   * Returns the sound instance of the requested sound file.
   * Returns the sound instance of a dummy if the file does not exist or ist not playable.
   * @param {string} id - path to sound relative to sounds/ folder
   * @return {object} the sound instance of the requested file or a dummy if the instance could not be created.
   * @memberof! MTLG.assets
   */
  a.getSoundInstance = function(id){
    var soundPath = "sounds/" + id + ".mp3";
    var instance;
    try{
      instance = createjs.Sound.createInstance(soundPath);
    }catch(e){
      console.log("Unable to create sound instance of path: " + soundPath);
      instance = instance = new createjs.DefaultSoundInstance(soundPath);
    }
    return instance;
  }

  /**
   * @function playSound -
   * Plays the requested sound and returns the sound instance.
   * @param {string} id - the path to the sound file relative to sounds/
   * @param {object} properties - the properties passed to the instance when it is played. See createjs docs for more information on available options.
   * @memberof! MTLG.assets
   */
  a.playSound = function(id, properties){
    var instance = a.getSoundInstance(id).play(properties);
    if(instance.playState == "playFailed"){ // This happens if the sound file does not exist
      console.log("Trying to play non existing sound instance: " + id);
      setTimeout(function(){ // Set timeout so that event "on" set after play() can be triggered
        instance.dispatchEvent("complete");
      });
    }
    return instance;
  }

  /**
   * @function getLangSoundInstance -
   * Returns the sound instance of the sound file id of the current language, see MTLG.lang for more information on language settings
   * @param {string} id - the sound file to be played missing the final "_lang" e.g. "someSound_en.mp3" would not include the "_en" and become "someSound"
   * @return {object} the created sound instance of a dummy if the file could not be played
   * @memberof! MTLG.assets
   */
  a.getLangSoundInstance = function(id){
    var filename = id + "_" + MTLG.lang.getLanguage();
    return a.getSoundInstance(filename);
  }

  /**
   * @function playLangSound -
   * Plays a language specific sound and returns the sound instance
   * @param {string} id - the sound file to be played missing the final "_lang" e.g. "someSound_en.mp3" would not include the "_en" and become "someSound"
   * @param {object} properties - the properties passed to the instance when it is played. See createjs docs for more information on available options.
   * @return {object} the created sound instance of a dummy if the file could not be played
   * @memberof! MTLG.assets
   */
  a.playLangSound = function(id, properties){
    var filename = id + "_" + MTLG.lang.getLanguage();
    //DEBUG
    //console.log(path);
    return a.playSound(filename, properties);
  }


  /* Background Music:
   * Handle Background Music seperately
   * To make it easier to keep track of the bgm instance
   */

  var backgroundMusicInstance;
  /**
   * @function playBackgroundMusic -
   * plays the specified sound file as bgm, i.e. it loops
   * @param {string} id - the path to the sound file
   * @memberof! MTLG.assets
   */
  a.playBackgroundMusic = function(id){
    if(backgroundMusicInstance){
      a.stopBackgroundMusic();
    }
    backgroundMusicInstance = a.playSound(id,{loop : -1});
  }

  /**
   * @function stopBackgroundMusic -
   * Stops the current background music, i.e. pauses and resets time
   * @memberof! MTLG.assets
   */
  //Stop registered BGM
  a.stopBackgroundMusic = function(){
    if(backgroundMusicInstance){
      backgroundMusicInstance.stop();
    }
  }

  /**
   * @function pauseBackgroundMusic -
   * Pause registered BGM
   * @memberof! MTLG.assets
   */
  a.pauseBackgroundMusic = function(){
    if(backgroundMusicInstance){
      backgroundMusicInstance.paused = true;
    }
  }

  /**
   * @function resumeBackgroundMusic -
   * Resume registered BGM
   * Can resume paused and stopped instances
   * @memberof! MTLG.assets
   */
  a.resumeBackgroundMusic = function(){
    if(backgroundMusicInstance){
      backgroundMusicInstance.paused = false;
    }
    //Handle stopped instances
    if(backgroundMusicInstance.playState === 'playFinished'){
      backgroundMusicInstance.play({loop : -1});
    }
  }

  /**
   * @function setBackgroundMusicVolume -
   * Sets the volume of the background music
   * @param {number} newVol - the new volume, should be a number in [0,1] where 0 is muted and 1 is normal volume.
   * @memberof! MTLG.assets
   */
  a.setBackgroundMusicVolume = function(newVol){
    if(backgroundMusicInstance){
      backgroundMusicInstance.volume = newVol;
    }
  }


  //Register the tangible module with the MTLG gameframe
  m.addModule(a.init);
  m.assets = a;

  return m;
}(MTLG));
