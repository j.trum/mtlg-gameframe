/**
 * @Author: thiemo
 * @Date:   2017-11-17T11:19:00+01:00
 * @Last modified by: Vincent
 * @Last modified time: 2017-11-20T16:33:10+01:00
 */



/**
 * This is the language module, MTLG.lang.
 * It functions like a conditional key-value storage,
 * that returns the value for a given key depending on
 * the current language.
 * Call 'define' with an object containing the language
 * as key and the key-value pairs as value.
 * Change the current language with 'setLanguage'.
 * Retrieve the language specific values with
 * 'getString', passing the key as parameter.
 * @namespace lang
 * @memberof MTLG
 */
var MTLG = (function (m){
  var lang = (function(){
    var currentLang = "en";
    var languages = {};
    /**
     * @function define -
     * Add one or more new key-value pairs to the storage, seperated into different languages
     * @param {object} obj - an object containing objects that are referenced by the language keys. The contained objects in turn contain the key- value pairs as objects. <br>
     *    Example: {"en": {"someKey": "someValue"}, "de": {"someKey": "einWert"}}
     * @memberof! MTLG.lang
     */
    var define = function(obj){
      var lang, langObj, key;
      for(lang in obj){
        langObj = obj[lang];
        languages[lang] = languages[lang] || {};
        for(key in langObj){
          languages[lang][key] = langObj[key];
        }
      }
    }

    /**
     * @function setLanguage -
     * Set the current language to the new language
     * @param {string} lang - the new language
     * @return {bool} - returns true if the language exists
     * @memberof! MTLG.lang
     */
    var setLanguage = function(lang){
      if(languages[lang]){
        currentLang = lang;
        return true;
      }else{
        console.log("Undefined language: " + lang);
        return false;
      }
    }

    /**
     * @function getLanguage -
     * Returns the current language
     * @return {string} - the currently selected language
     * @memberof! MTLG.lang
     */
    var getLanguage = function(){
      return currentLang;
    }

    /**
     * @function getString -
     * Returns the value of the given key of the current language
     * @param {string} key - the key which value should be returned
     * @return  Returns the value the key maps to of the language currently selected
     * @memberof! MTLG.lang
     */
    var getString = function(key){
      if(!languages[currentLang][key]){
        console.log("LangHandler: Possibly unknown key: " + key);
        return key;
      }
      return languages[currentLang][key];
    }

    return {
      define : define,
      setLanguage : setLanguage,
      getLanguage : getLanguage,
      getString : getString
    }
  })();

  m.lang = lang;
  return m;
})(MTLG);
